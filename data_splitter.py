#!/usr/bin/python3

import pandas as pd
from sklearn.model_selection import train_test_split

with open('movies_data.csv', encoding="utf8") as csv_file:
	csv_reader = pd.read_csv(csv_file, delimiter=',')
	X_train, X_test_dev = train_test_split(csv_reader ,test_size=0.2, random_state=120)	
	X_test, X_dev = train_test_split(X_test_dev ,test_size=0.5, random_state=120)	
	X_train.to_csv("training.csv", sep=',', encoding='utf-8',index=False)
	X_test.to_csv("test.csv", sep=',', encoding='utf-8',index=False)
	X_dev.to_csv("developer.csv", sep=',', encoding='utf-8',index=False)
