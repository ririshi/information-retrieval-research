#!/usr/bin/python3

import pickle
import csv
from collections import defaultdict
from sklearn.preprocessing import OneHotEncoder

with open('movies_data.csv', encoding="utf8") as csv_file:
		final_dict = defaultdict(list)
		csv_reader = csv.reader(csv_file, delimiter=',')
		for line in csv_reader:
			if line[2] == "Comedy": 
				final_dict["Comedy"].append((line[0],line[1]))
			if line[2] == "Thriller":
				final_dict["Thriller"].append((line[0],line[1]))
			if line[2] == "Drama":
				final_dict["Drama"].append((line[0],line[1]))
movies_pickle = open("movies.pickle","wb")
pickle.dump(final_dict,movies_pickle)
movies_pickle.close()

